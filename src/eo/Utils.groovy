package eo;

def jabber_notify(currentBuild, env, groupchat = null) {
    if (currentBuild.result == null || currentBuild.result == 'SUCCESS') {
        return;
    }
    message = "$currentBuild.fullDisplayName build failed: $currentBuild.absoluteUrl"
    if (env.GIT_BRANCH == 'origin/main' && groupchat) {
        sh("echo '$message' | sendxmpp -c $groupchat")
    } else {
        author = sh(returnStdout: true, script: "/usr/bin/git show -s --format='%ae' HEAD | sed s/@entrouvert.*//").trim()
        sh("echo '$message' | sendxmpp $author@im.libre-entreprise.com")
    }
}

def mail_notify(currentBuild, env, email) {
    /* Send mail to 'email' if branch is main
     else send mail to the author of the last commit
     */
    if (currentBuild.result == null) {
        // Hack to have the 'jenkins build back to normal' mail sent
        currentBuild.result = 'SUCCESS'
    }
    if (env.GIT_BRANCH == 'origin/main') {
        step([
            $class: 'Mailer', notifyEveryUnstableBuild: true,
            recipients: email, sendToIndividuals: true
        ])
    } else {
        author = sh(returnStdout: true, script: "/usr/bin/git show -s --format='%ae' HEAD").trim()
        step([
            $class: 'Mailer', notifyEveryUnstableBuild: true,
            recipients: author, sendToIndividuals: true
        ])
    }
}


def publish_coverage(report_pattern) {
    step([
        $class: 'CoberturaPublisher', autoUpdateHealth: false, autoUpdateStability: false,
         coberturaReportFile: report_pattern, failUnhealthy: false, failUnstable: false,
         maxNumberOfBuilds: 0, onlyStable: false, sourceEncoding: 'ASCII', zoomCoverageChart: false
    ])
}


def publish_coverage_native(
    report_pattern = 'index.html', report_dir = 'htmlcov',
    report_name = 'Coverage Report (native)') {
    publishHTML target : [
        allowMissing: false, alwaysLinkToLastBuild: false, keepAll: true, reportDir: report_dir,
        reportFiles: report_pattern, reportName: report_name,reportTitles: ''
    ]
}


def publish_pylint(report_pattern) {
    recordIssues enabledForFailure: true, tool: pyLint(pattern: report_pattern)
}


return this
