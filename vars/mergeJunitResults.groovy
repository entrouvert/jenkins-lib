def request = libraryResource 'com/mycorp/pipeline/somelib/request.json'

def call() {
    tmpdir = env.WORKSPACE_TMP ? env.WORKSPACE_TMP : '.'
    writeFile file:"${tmpdir}/merge-junit-results.py", text: libraryResource('merge-junit-results.py')
    sh "python3 ${tmpdir}/merge-junit-results.py junit-*.xml >junit.xml"
    sh 'rm junit-*.xml'
    junit 'junit.xml'
}
