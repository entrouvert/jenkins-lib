def call() {
    tmpdir = env.TMPDIR ? env.TMPDIR : '.'
    sh """
virtualenv -p python3 ${tmpdir}/venv/
${tmpdir}/venv/bin/pip install tox
PGPORT=`python3 -c 'import struct; import socket; s=socket.socket(); s.setsockopt(socket.SOL_SOCKET, socket.SO_LINGER, struct.pack("ii", 1, 0)); s.bind(("", 0)); print(s.getsockname()[1]); s.close()'` pg_virtualenv -o fsync=off ${tmpdir}/venv/bin/tox -rv"""
}
